import axios from 'axios';
import store from '@/store';
import JSONBig from 'json-bigint';

// const atricleJson = '{ "art_id": 125575199582270265552 }';
// console.log(JSON.parse(atricleJson)); // {id: 125575199582270260000}
// console.log(JSONBig.parse(atricleJson)); // {id: BigNumber}
// console.log('-------------------');
// console.log(JSON.parse(atricleJson).art_id); // 125575199582270260000
// console.log(JSONBig.parse(atricleJson).art_id.toString()); // 125575199582270265552
// console.log('-------------------');
// console.log(JSON.stringify(JSON.parse(atricleJson))); // {"id":1255751995822702600}
// console.log(JSONBig.stringify(JSONBig.parse(atricleJson))); // {"art_id":125575199582270265552}

const request = axios.create({
  // 基准路径
  baseURL: 'http://ttapi.research.itcast.cn/',
  // 自定义后端返回的原始数据
  // data: 后端返回的原始数据，说白了就是 JSON 格式的字符串
  transformResponse: [
    function(data) {
      try {
        return JSONBig.parse(data);
      } catch {
        return data;
      }
      // axios 默认会在内部这样来处理后端返回的数据
      // return JSON.parse(data)
    }
  ]
});

// 请求拦截器
request.interceptors.request.use(
  config => {
    // 请求发起会经过这里
    // config：本次请求的请求配置对象
    const { user } = store.state;
    if (user && user.token) {
      config.headers.Authorization = `Bearer ${store.state.user.token}`;
    }

    return config;
  },
  err => {
    return Promise.reject(err);
  }
);

export default request;
