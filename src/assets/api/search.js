/**
 * 用户相关请求模块
 */
import request from '@/assets/utils/request';

/**
 * 联想数据
 */
export const getSearchSuggestions = q => {
  return request({
    method: 'GET',
    url: '/app/v1_0/suggestion',
    params: {
      q
    }
  });
};

/**
 * 搜索结果
 */
export const getSearchResult = params => {
  return request({
    method: 'GET',
    url: '/app/v1_0/search',
    params: params
  });
};
