import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
// vant 配置
import './plugins/vant.js';
// 加载动态设置 REM 基准值
import 'amfe-flexible';
// 样式
import './assets/styles/index.less';
// dayjs
import '@/assets/utils/dayjs';

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
